<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<title>Insert title here</title>
</head>
<body>

	<%@page import="eus.zornotza.Tarjeta"%>
	<%@page import="java.util.ArrayList"%>
	<%
		ArrayList<Tarjeta> lista = (ArrayList<Tarjeta>) session.getAttribute("lista_sesion");
		
		if (lista == null) {

			lista = new ArrayList<Tarjeta>();
			session.setAttribute("lista_sesion", lista);
		}

		String numero = request.getParameter("numero");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String CCV = request.getParameter("CCV");
		String name = request.getParameter("name");
		Boolean save = Boolean.parseBoolean(request.getParameter("save"));

		out.println("<br/><br/><br/>");
		out.println("<h1>TARJETA ACEPTADA</h1>");
		out.println("<br/><br/><br/>");

		if ((numero != null) && (month != null) && (year != null) && (CCV != null) && (name != null)) {

			Tarjeta tarjeta = new Tarjeta(numero, month, year, CCV, name, save);
			lista.add(tarjeta);

		} else {
			out.println("<h2>" + "RELLENA TODOS LOS CAMPOS" + "</h2>");
		}
	%>
	<table class="table">
		<thead>
			<tr>
				<th>NAME</th>
				<th>NUMBER</th>
				<th>Expiry Month</th>
				<th>Expiry Year</th>
				<th>CCV</th>
			</tr>
		</thead>
		<%
			for (Tarjeta tarjeta : lista) {
		%>
		<tr class="success">
			<td>
				<%
					out.println(tarjeta.getName());
				%>
			</td>
			<td>
				<%
					out.println(tarjeta.getNumero());
				%>
			</td>
			<td>
				<%
					out.println(tarjeta.getMonth());
				%>
			</td>
			<td>
				<%
					out.println(tarjeta.getYear());
				%>
			</td>
			<td>
				<%
					out.println(tarjeta.getCCV());
				%>
			</td>

		</tr>
		<%
			}
		%>

	</table>

</body>
</html>