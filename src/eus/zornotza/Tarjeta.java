package eus.zornotza;

public class Tarjeta {

	String numero;
	String month;
	String year;
	String CCV;
	String name;
	Boolean save;
	
	
	public Tarjeta() {
		super();
	}
	public Tarjeta(String numero, String month, String year, String cCV, String name, Boolean save) {
		super();
		this.numero = numero;
		this.month = month;
		this.year = year;
		CCV = cCV;
		this.name = name;
		this.save = save;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getCCV() {
		return CCV;
	}
	public void setCCV(String cCV) {
		CCV = cCV;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getSave() {
		return save;
	}
	public void setSave(Boolean save) {
		this.save = save;
	}
	
	
}
